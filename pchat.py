#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys
import time
import json
import signal
import socket
import psutil
import select
import pickle
import struct

marshall = pickle.dumps
unmarshall = pickle.loads


def kill_proc_tree(pid, including_parent=True):
    parent = psutil.Process(pid)
    if including_parent:
        parent.kill()


def send(channel, *msg):
    buf = marshall(msg)
    value = socket.htonl(len(buf))
    size = struct.pack("L", value)
    channel.send(size)
    channel.send(buf)


def receive(channel):

    size = struct.calcsize("L")
    # print("note2")
    size = channel.recv(size)
    try:
        size = socket.ntohl(struct.unpack("L", size)[0])
    except struct.error as e:
        return ''

    buf = ""

    while len(buf) < size:
        buf = channel.recv(size - len(buf))

    return unmarshall(buf)[0]
